import React from 'react';

export default class TransactionTableRow extends React.Component
{
    constructor()
    {
        super(...arguments);
    }

    render()
    {
        let _this = this;
        let bankName = this.props.banks.map((bank) => {
            if(bank.bankId == _this.props.bankId){
                return bank.bankName;
            }
        });
        return(
            <tr>
                <td>{this.props.id}</td>
                <td>{bankName}</td>
                <td>{this.props.amount}</td>
                <td>
                    <span className="glyphicon glyphicon-remove" onClick={ () => { this.props.func(this.props.id)} }></span>
                </td>
            </tr>
        );
    }
}