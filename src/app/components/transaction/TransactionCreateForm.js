import React from 'react';
import {browserHistory} from 'react-router';

import BanksStore from '../../stores/BankStore';
import {fetchBanks} from '../../actions/bankActions';
import {addTransaction} from '../../actions/transactionActions';

export default class TransactionCreateForm extends React.Component
{
    constructor()
    {
        super(...arguments);

        this.state = {
            banks: []
        };

        this.onBanksChange = this.onBanksChange.bind(this);
        this.newTransactions = this.newTransactions.bind(this);
    }

    newTransactions()
    {
        let amountInput = $('[name="amount"]');
        if(amountInput.val() !== ''){
            let bankId = $('[name="bankId"]').val();
            let amount = amountInput.val();
            let transaction = { bankId, amount };
            addTransaction(transaction);
            browserHistory.push('/transactions');
        }else{
            amountInput.parent().addClass('has-error');
            setTimeout(() => {
                amountInput.parent().removeClass('has-error');
            }, 500)
        }
    }

    onBanksChange(banks)
    {
        this.setState({
            banks
        });
    }

    componentDidMount()
    {
        fetchBanks();
    }

    componentWillMount(){
        BanksStore.on('change', this.onBanksChange);
    }

    componentWillUnmount()
    {
        BanksStore.removeListener('change', this.onBanksChange);

    }

    render()
    {
        let selectOptions = null;
        if(this.state.banks.length > 0){
            selectOptions = this.state.banks.map((bank, index) => {
                return <option value={bank.bankId} key={index}>{bank.bankName}</option>;
            });
        }
        return(
            <div>
                <h3 className="text-center">Добавление транакции</h3>
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <form id="add_transaction_form">
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Выберите банк</label>
                                <select className="form-control" name="bankId">
                                    {selectOptions}
                                </select>
                                <small className="form-text text-muted">Поле является обязательным.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Размер транзакции</label>
                                <input type="number" className="form-control" name="amount" required="required" />
                                <small className="form-text text-muted">Будьте внимательны! Операция необратима.</small>
                            </div>
                            <a className="btn btn-primary" onClick={this.newTransactions}>Добавить</a>
                        </form>
                    </div>
                    <div className="col-md-3"></div>
                </div>
            </div>
        );
    }
}