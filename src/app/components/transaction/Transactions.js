import React from 'react';
import {Link} from 'react-router';

import TransactionTable from './TransactionTable';
import TransactionStore from '../../stores/transactionStore';
import BanksStore from '../../stores/BankStore';
import {deleteTransaction,addTransaction,fetchTrasactions} from '../../actions/transactionActions';
import {fetchBanks} from '../../actions/bankActions';


export default class Transactions extends React.Component {
    constructor() {
        super(...arguments);

        this.state = {
            transactions: [],
            banks: []
        };

        this.removeTransaction = this.removeTransaction.bind(this);
        this.onTransactionsChange = this.onTransactionsChange.bind(this);
        this.onBanksChange = this.onBanksChange.bind(this);
    }

    static onEnter(nextState, replace) {
        const login = window.localStorage.getItem('user_login');
        if(login !== 'admin'){
            replace('/login');
        }else{
            if(window.location.pathname === '/'){
                replace('/transactions');
            }
        }
    }

    removeTransaction(id)
    {
        deleteTransaction(id);
    }

    onTransactionsChange(transactions)
    {
        this.setState({
            transactions
        });
    }

    onBanksChange(banks)
    {
        this.setState({
            banks
        });
    }

    componentDidMount()
    {
        fetchBanks();
        fetchTrasactions();
    }

    componentWillMount(){
        TransactionStore.on('change', this.onTransactionsChange);
        BanksStore.on('change', this.onBanksChange);
    }

    componentWillUnmount()
    {
        TransactionStore.removeListener('change', this.onTransactionsChange);
        BanksStore.removeListener('change', this.onBanksChange);

    }

    render() {
        return(
            <div>
                <Link to='/transactions/create' className="btn btn-primary">
                    Добавить транзакцию
                </Link>
                <TransactionTable
                    transactions={this.state.transactions}
                    removeFunc={this.removeTransaction}
                    banks={this.state.banks}
                />
            </div>
        );
    }
}