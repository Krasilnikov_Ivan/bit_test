import React from 'react';
import Transaction from './TransactionTableRow';


export default class TransactionTable extends React.Component {
    constructor() {
        super(...arguments);
    }

    render() {
        if(!this.props.transactions.length){
            return null;
        }

        let _this = this;
        let tableTbody = this.props.transactions.map((transaction, index) => {
            return <Transaction
                key={index}
                {...transaction}
                func={_this.props.removeFunc}
                banks={_this.props.banks}
            />
        });

        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Номер транзакции</th>
                        <th>Банк</th>
                        <th>Стоимость</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {tableTbody}
                </tbody>
            </table>
        );
    }
}