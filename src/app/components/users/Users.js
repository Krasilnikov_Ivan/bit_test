import React from 'react';
import {browserHistory} from 'react-router';

import AuthForm from './AuthForm';
import UserStore from '../../stores/userStore';
import {userLogin} from '../../actions/userActions';


export default class Users extends React.Component {
    constructor() {
        super(...arguments);

        this.state = {
            users: [],
            login: false
        };

        this.login = this.login.bind(this);
        this.onUsersChange = this.onUsersChange.bind(this);
    }

    static onEnter(nextState, replace) {
        const login = window.localStorage.getItem('user_login');
        if (login == 'admin') {
            replace('/transactions');
        }
    }

    login()
    {
        let  form = $('#auth_form');
        let formInputs = form.find('input');
        let flag = true;
        for(let i = 0; i < formInputs.length; i++){
            if($(formInputs[i]).val() == ''){
                flag = false;
                $(formInputs[i]).parent().addClass('has-error');
                setTimeout(function () {
                    $(formInputs[i]).parent().removeClass('has-error');
                }, 500);
            }
        }
        if(flag){
            userLogin(form.serialize());
        }
    }

    onUsersChange(userData)
    {
        this.setState({
            login: userData.login,
            user: userData.user
        });
        browserHistory.push('/transactions');
    }

    componentWillMount(){
        UserStore.on('change', this.onUsersChange);
    }

    componentDidMount()
    {
        Users.onEnter();
    }

    componentWillUnmount()
    {
        UserStore.removeListener('change', this.onUsersChange);
    }

    render() {
        return(
            <AuthForm
                login={this.state.login}
                loginFunc={this.login}
            />
        );
    }
}