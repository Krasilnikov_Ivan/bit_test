import React from 'react';
import {userLogout} from '../../actions/userActions';

export default class Logout extends React.Component
{
    constructor()
    {
        super(...arguments);
    }

    static onEnter(nextState, replace) {
        window.localStorage.removeItem('user_login');
        userLogout();
        replace('/login');
    }

    render()
    {
        return(
            <div></div>
        );
    }
}