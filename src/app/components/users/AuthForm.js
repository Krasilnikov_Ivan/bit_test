import React from 'react';

export default class AuthForm extends React.Component
{
    constructor()
    {
        super(...arguments);
    }

    render()
    {
        return (
            <div>
                <h1 className="text-center">Добро пожаловать!</h1>
                <h3 className="text-center">Для использования приложения необходима авторизация</h3>
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <form id="auth_form">
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Ваш логин</label>
                                <input type="text" className="form-control" name="user_login" placeholder="login" required="required" />
                                <small className="form-text text-muted">Мы никогда не передаем ваши данные третьим лицам.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Ваш пароль</label>
                                <input type="password" className="form-control" name="user_pass" placeholder="pass" required="required" />
                                <small className="form-text text-muted">Ваша безопасность, в первую очередь, зависит от Вас. Выбирайте надёжный пароль.</small>
                            </div>
                            <a className="btn btn-primary" onClick={this.props.loginFunc}>Войти</a>
                        </form>
                    </div>
                    <div className="col-md-3"></div>
                </div>
            </div>
        );
    }
}