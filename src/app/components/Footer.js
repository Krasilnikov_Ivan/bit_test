import React from 'react';


export default class Footer extends React.Component {
    constructor() {
        super(...arguments);
    }

    render() {
        return (
            <footer className="footer">
                <div className="container">
                    <p className="text-muted">
                        {this.props.children}
                    </p>
                </div>
            </footer>
        );
    }
}