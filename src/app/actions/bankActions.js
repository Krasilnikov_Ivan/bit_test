import dispatcher from '../dispatcher';
import {
    FETCH_BANK_START,
    ADD_BANK,
    DELETE_BANK
} from '../constants/bankConstants';

export function fetchBanks() {
    dispatcher.dispatch({
        type: FETCH_BANK_START
    })
}