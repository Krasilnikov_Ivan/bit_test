import dispatcher from '../dispatcher';
import {
    FETCH_TRANSACTIONS_START,
    ADD_TRANSACTION,
    DELETE_TRANSACTION
} from '../constants/transactionConstants';

export function addTransaction({bankId, amount}) {
    const transaction = {bankId, amount};

    dispatcher.dispatch({
        type: ADD_TRANSACTION,
        payload: transaction
    })
}

export function fetchTrasactions() {
    dispatcher.dispatch({
        type: FETCH_TRANSACTIONS_START
    })
}

export function deleteTransaction(id) {
    dispatcher.dispatch({
        type: DELETE_TRANSACTION,
        payload: id
    })
}