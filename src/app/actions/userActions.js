import dispatcher from '../dispatcher';
import {USER_LOGIN_START, USER_LOGOUT} from '../constants/userConstants';

export function userLogin(queryParams) {
    dispatcher.dispatch({
        type: USER_LOGIN_START,
        payload: queryParams
    });
}

export function userLogout() {
    dispatcher.dispatch({
        type: USER_LOGOUT
    });
}