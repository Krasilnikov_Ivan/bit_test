export const FETCH_BANK_START = 'FETCH_BANK_START';
export const FETCH_BANK_END = 'FETCH_BANK_END';
export const ADD_BANK = 'ADD_BANK';
export const DELETE_BANK = 'DELETE_BANK';