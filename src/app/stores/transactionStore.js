import {
    FETCH_TRANSACTIONS_START,
    FETCH_TRANSACTIONS_END,
    ADD_TRANSACTION,
    DELETE_TRANSACTION
} from '../constants/transactionConstants';
import transactionsData from '../data/transactionsData.json';

import dispatcher from '../dispatcher';

import axios from 'axios';

import {EventEmitter} from 'events';

class TransactionStore extends EventEmitter
{
    constructor()
    {
        super(...arguments);

        this.transactions = [];

        this.fetchTransactionsStart = this.fetchTransactionsStart.bind(this);
        this.fetchTransactionsEnd = this.fetchTransactionsEnd.bind(this);
        this.change = this.change.bind(this);
        this.getTransactions = this.getTransactions.bind(this);
        this.getTransactionMaxIndex = this.getTransactionMaxIndex.bind(this);
        this.handleActions = this.handleActions.bind(this);
    }

    fetchTransactionsStart(){
        axios
            .get('http://localhost:8083/api/transactions/')
            .then((response) => {
                let { data } = response;

                dispatcher.dispatch({
                    type: FETCH_TRANSACTIONS_END,
                    payload: data
                });
            }).catch((response) => {
                dispatcher.dispatch({
                    type: FETCH_TRANSACTIONS_END,
                    payload: window.localStorage.getItem('transactions') ? JSON.parse(window.localStorage.getItem('transactions')) : transactionsData
                });
            });
    }

    fetchTransactionsEnd(transactions)
    {
        this.transactions = transactions;
        this.change();
    }

    change()
    {
        window.localStorage.setItem('transactions', JSON.stringify(this.transactions));
        this.emit('change', this.transactions);
    }

    getTransactions()
    {
        return this.transactions;
    }

    addTransaction(transaction)
    {
        let id = this.getTransactionMaxIndex() + 1;
        let bankId = transaction.bankId;
        let amount = transaction.amount;
        let newTransaction = {id, amount, bankId };
        this.transactions.push(newTransaction);
        this.change();
    }

    getTransactionMaxIndex()
    {
        return this.transactions[this.transactions.length - 1].id;
    }

    deleteTransactions(id)
    {
        let transactionIndex = 0;
        this.transactions.map((transaction, index) => {
            if(transaction.id == id){
                transactionIndex = index;
            }
        });
        this.transactions.splice(transactionIndex, 1);
        this.change();
    }

    handleActions(action)
    {
        switch (action.type)
        {
            case ADD_TRANSACTION:
            {
                this.addTransaction(action.payload);
                break;
            }
            case DELETE_TRANSACTION:
            {
                this.deleteTransactions(action.payload);
                break;
            }
            case FETCH_TRANSACTIONS_START:
            {
                this.fetchTransactionsStart();
                break;
            }
            case FETCH_TRANSACTIONS_END:
            {
                this.fetchTransactionsEnd(action.payload);
                break;
            }
        }
    }
}

const transactionsStore = new TransactionStore;

dispatcher.register(transactionsStore.handleActions);

export default transactionsStore;