import {
    FETCH_BANK_START,
    FETCH_BANK_END,
    ADD_BANK,
    DELETE_BANK
} from '../constants/bankConstants';
import banksData from '../data/bankNameData.json';

import dispatcher from '../dispatcher';

import axios from 'axios';

import {EventEmitter} from 'events';

class BankStore extends EventEmitter
{
    constructor()
    {
        super(...arguments);

        this.banks = [];

        this.fetchBankStart = this.fetchBankStart.bind(this);
        this.fetchBankEnd = this.fetchBankEnd.bind(this);
        this.change = this.change.bind(this);
        this.getBank = this.getBank.bind(this);
        this.handleActions = this.handleActions.bind(this);
    }

    fetchBankStart(){
        axios
            .get('http://localhost:8083/api/banks/')
            .then((response) => {
                let { data } = response;

                dispatcher.dispatch({
                    type: FETCH_BANK_END,
                    payload: data
                });
            }).catch((response) => {
                dispatcher.dispatch({
                    type: FETCH_BANK_END,
                    payload: banksData
                });
            });
    }

    fetchBankEnd(banks)
    {
        this.banks = banks;
        this.change();
    }

    change()
    {
        this.emit('change', this.banks);
    }

    getBank()
    {
        return this.banks;
    }

    addBank(transaction)
    {
        this.banks.push(transaction);
        this.change();
    }

    deleteBank(id)
    {
        this.banks = this.banks.map((transaction) => {
            return transaction.id !== id;
        });
        this.change();
    }

    handleActions(action)
    {
        switch (action.type)
        {
            case ADD_BANK:
            {
                this.addBank(action.payload);
                break;
            }
            case DELETE_BANK:
            {
                this.deleteBank(action.payload);
                break;
            }
            case FETCH_BANK_START:
            {
                this.fetchBankStart();
                break;
            }
            case FETCH_BANK_END:
            {
                this.fetchBankEnd(action.payload);
                break;
            }
        }
    }
}

const banksStore = new BankStore;

dispatcher.register(banksStore.handleActions);

export default banksStore;