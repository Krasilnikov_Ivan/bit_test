import {
    USER_LOGIN_START,
    USER_LOGIN_END,
    USER_LOGOUT
} from '../constants/userConstants';
import userData from '../data/userData.json';

import dispatcher from '../dispatcher';
import axios from 'axios';

import { EventEmitter } from 'events';

class UserStore extends EventEmitter {
    constructor()
    {
        super(...arguments);

        this.user = [];
        this.authStatus = false;

        this.userLoginStart = this.userLoginStart.bind(this);
        this.userLoginEnd = this.userLoginEnd.bind(this);
        this.userLogout = this.userLogout.bind(this);
        this.change = this.change.bind(this);
        this.getUser = this.getUser.bind(this);
        this.getAuthStatus = this.getAuthStatus.bind(this);
        this.handleActions = this.handleActions.bind(this);
    }

    userLoginStart(userParams)
    {
        axios
            .post('http://localhost:8083/api/users/login',{
                params: {
                    queryParams: userParams
                }
            })
            .then((response) => {
                let { data } = response;

                dispatcher.dispatch({
                    type: USER_LOGIN_END,
                    payload: data
                });
            }).catch((response) => {
                // Из-за того, что сервера нет, а авторизацию проверсти надо
                // я принудительно вызову успешное завершение запроса при совпадении логина и пароля
                let user = userParams.split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
                if(user.user_login == 'admin' && user.user_pass == '1234'){
                    dispatcher.dispatch({
                        type: USER_LOGIN_END,
                        payload: userData
                    });
                }
            });
        this.change();
    }

    userLoginEnd(response){
        this.user =  response.user;
        this.authStatus = response.status;
        window.localStorage.setItem('user_login', response.user.username);
        this.change();
    }

    userLogout()
    {
        axios
            .post('http://localhost:8083/api/users/logout')
            .then(() => {
                this.user = [];
            });
        this.change();
    }

    change()
    {
        this.emit('change', {user: this.user, login:this.authStatus});
    }

    getUser()
    {
        return this.user;
    }

    getAuthStatus()
    {
        return this.authStatus;
    }

    handleActions(action)
    {
        switch (action.type)
        {
            case USER_LOGIN_START:
            {
                this.userLoginStart(action.payload);
                break;
            }
            case USER_LOGIN_END:
            {
                this.userLoginEnd(action.payload);
                break;
            }
            case USER_LOGOUT:
            {
                this.userLogout();
                break;
            }
        }
    }
}

const userStore = new UserStore;

dispatcher.register(userStore.handleActions);

export default userStore;