import React from 'react';

import Menu from '../components/menu/Menu';
import MenuItem from '../components/menu/MenuItem';
import Footer from '../components/Footer';

export default class Layout extends React.Component {
    constructor()
    {
        super(...arguments);

        this.state = {
            visibleLink: window.localStorage.getItem('user_login') == 'admin' ? true : false
        };

        this.brand = 'Transactions';
    }

    isActive(href)
    {
        return window.location.pathname == href;
    }

    componentDidMount()
    {
        this.checkState();
    }

    // Чтобы своевременно обновлялась отрисовка меню
    checkState()
    {
        this.setState({
            visibleLink: window.localStorage.getItem('user_login') == 'admin' ? true : false
        });
        setTimeout(this.checkState.bind(this), 500);
    }

    render() {
        return (
            <div>
                <Menu brand={this.brand}>
                    <MenuItem href="/transactions" active={this.isActive('/transactions')} visible={this.state.visibleLink}>
                        Транзакции
                    </MenuItem>
                    <MenuItem href="/transactions/create" active={this.isActive('/transactions/create')} visible={this.state.visibleLink}>
                        Добавить транзакцию
                    </MenuItem>
                    <MenuItem href="/logout" active={this.isActive('/logout')} visible={this.state.visibleLink}>
                        Выйти
                    </MenuItem>
                </Menu>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            { this.props.children }
                        </div>
                    </div>
                </div>
                <Footer>
                    &copy; 2017 Красильников Иван
                </Footer>
            </div>
        );
    }
}