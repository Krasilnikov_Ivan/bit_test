import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, browserHistory, Redirect} from 'react-router';

import MainLayout from './app/layouts/MainLayout';
import Users from './app/components/users/Users';
import UsersLogout from './app/components/users/Logout';
import Transactions from './app/components/transaction/Transactions';
import TransactionCreateForm from './app/components/transaction/TransactionCreateForm';

const app = document.getElementById('app');

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={MainLayout}>
            <IndexRoute component={Transactions} onEnter={Transactions.onEnter}/>
            <Route path='transactions' component={Transactions} onEnter={Transactions.onEnter}></Route>
            <Route path="transactions/create" component={TransactionCreateForm}/>
            <Route path="login" component={Users} onEnter={Users.onEnter}/>
            <Route path="logout" component={UsersLogout} onEnter={UsersLogout.onEnter}/>
        </Route>
    </Router>,
    app
);
